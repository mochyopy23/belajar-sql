Soal 1 Membuat Database
create database myshop;

Soal 2 Membuat Table di Dalam Database
create table users(id int auto_increment, name varchar(255), email varchar(255), password varchar(255), primary key(id));
create table items(id int auto_increment, name varchar(255), description varchar(255), price int, stock int, category_id int, primary key(id),foreign key(category_id) references categories(id));
create table categories(id int auto_increment, name varchar(255), primary key(id));

Soal 3 Memasukkan Data pada Table
table users
insert into users(name,email,password) values("John Doe","john@doe.com","john123");
insert into users(name,email,password) values("Jane Doe","jane@doe.com","jenita123");

table categories
insert into categories(name) values("gadget");
insert into categories(name) values("cloth");
insert into categories(name) values("men");
insert into categories(name) values("women");
insert into categories(name) values("branded");

table items
insert into items(name,description,price,stock,category_id) values("Sumsang b50","hape keren dari merek sumsang",4000000,100,1);
insert into items(name,description,price,stock,category_id) values("Uniklooh","baju keren dari brand ternama",500000,50,2);
insert into items(name,description,price,stock,category_id) values("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

Soal 4 Mengambil Data dari Database
a. Mengambil data users
Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.
select id,name,email from users;

b. Mengambil data items
Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
select * from items where price >1000000;

Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
select * from items where name like "%uniklo%";

c. Menampilkan data items join dengan kategori
Buatlah sebuah query untuk menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items.
select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;

Soal 5 Mengubah Data dari Database
Ubahlah data pada table items untuk item dengan nama sumsang harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 5.
update items set price=2500000 where name like "%sumsang%";





































table items
